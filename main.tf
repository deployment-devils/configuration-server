terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
    linode = {
      source = "linode/linode"
      version = "2.5.2"
    }
  }
  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "eu-north-1"
}

resource "aws_instance" "app-server" {
  ami                    = "ami-0506d6d51f1916a96"
  instance_type          = "t3.micro"
  key_name               = "Deployment-devils-aws"
  vpc_security_group_ids = ["sg-0b25f947adf76c5c8"]

  tags = {
    Name = "NewsApp-backend"
  }
}

resource "aws_instance" "app-server2" {
  ami                    = "ami-0506d6d51f1916a96"
  instance_type          = "t3.micro"
  key_name               = "Deployment-devils-aws"
  vpc_security_group_ids = ["sg-0b25f947adf76c5c8"]

  tags = {
    Name = "NewsApp-frontend"
  }
}

resource "aws_instance" "app-server3" {
  ami                    = "ami-0506d6d51f1916a96"
  instance_type          = "t3.micro"
  key_name               = "Deployment-devils-aws"
  vpc_security_group_ids = ["sg-0b25f947adf76c5c8"]

  tags = {
    Name = "NewsApp-Runners"
  }
}

resource "aws_instance" "app-server4" {
  ami                    = "ami-0506d6d51f1916a96"
  instance_type          = "t3.micro"
  key_name               = "Deployment-devils-aws"
  vpc_security_group_ids = ["sg-0b25f947adf76c5c8"]

  tags = {
    Name = "Grafana-Prometheus"
  }
}

provider "linode" {
  token = "d0c35b04c99f9d8e1ac751e6215faa6b56e999d55844aa49c183341d4a72ac64"
}

resource "linode_instance" "terraform-sonarqube" {
        image = "linode/ubuntu18.04"
        region = "se-sto"
        type = "g6-standard-2"
        authorized_keys = ["ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiBxRRkmAGwQuTVHa9ZqxPlNpVfDO+ch/l30TpJjCbBmbgL7Equ8XabuZKYlAbLllfAbL0uVLtmz/DVckwnzd8+l4vjaXxLlTtyN5uF72dQ22BeAkydV2T9ZXpYv6prl3kRKAqLFKySEGVm0Lk7aS1qf6TLIMdZVzAn/87kReVWr9TCzjRQv+7YjNyziJVCDwOVURBQsJohTp5kP0Lhh2xGva90E7LutbGJhSupukx+IgonABT6Ob4P4lCeFjKEul9i0PjnHZLRSpbgsQsdQHmEVXQ1+oPPEr4voaIXG6ds5f33rsdcw3UAXia8lnnO4xaEbRud7E8h5spNfqAkCNr engineer@debian"]
        root_pass = "Dep@devils2024!"
}


